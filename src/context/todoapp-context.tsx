import React, { createContext, useReducer, Dispatch, FC } from 'react';
import { ITodo } from '../shared/interfaces/todo.interface';
import { TASK_STATUS } from '../shared/enums/task-status';
import { Action } from '../shared/interfaces/action.interface';
import { ACTION_TYPES } from '../shared/enums/action-types';
import { v4 as uuid } from 'uuid';

type TodoContextState = {
    todos: ITodo[];
    expanded: string | false;
    statusFilter: TASK_STATUS;
}

type ContextType = { 
    state: TodoContextState;
    dispatch: Dispatch<Action>;
};

const initialState: TodoContextState = {
    todos: [],
    expanded: false,
    statusFilter: TASK_STATUS.TODO,
}

const initialContext: ContextType = {
    state: initialState,
    dispatch: () => {},
}

export const TodoContext = createContext<ContextType>(initialContext);

const reducer = (state: TodoContextState, action: Action): TodoContextState => {
    let { todos } = state;
    let { payload: todo } = action;
    switch(action.type) {
        case ACTION_TYPES.ADD_TODO:
            let { todos:currentTodos } = state;
            return {
                ...state,
                todos: [...currentTodos, {...action.payload as ITodo, id: uuid()}] ,
            }
        case ACTION_TYPES.START_TODO:
            return {
                ...state,
                todos: todos.map(item => {
                    if(item.id === todo.id) {
                      return {...item, status: TASK_STATUS.DOING }
                    }
                    return {...item};
                }),
            }
        
        case ACTION_TYPES.FINISH_TODO:
            return {
                ...state,
                todos: todos.map(item => {
                    if(item.id === todo.id) {
                        return {...item, status: TASK_STATUS.DONE }
                    }
                    return {...item};
                }),
            }
        default:
            return {...state};
    }
}

export const TodoProvider: FC = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);

    return (
        <TodoContext.Provider value={{state, dispatch}}>
            { children }
        </TodoContext.Provider>
    );
}