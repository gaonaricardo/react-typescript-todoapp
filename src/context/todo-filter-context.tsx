import React, { FC, useState, Dispatch, SetStateAction, createContext } from 'react';
import { TASK_STATUS } from '../shared/enums/task-status';

type TodoFilterContextType = {
    statusFilter: TASK_STATUS;
    setStatusFilter: Dispatch<SetStateAction<TASK_STATUS>>;
}

export const TodoFilterContext = createContext<TodoFilterContextType>({
    statusFilter: TASK_STATUS.TODO,
    setStatusFilter: () => {},
});

export const TodoFilterProvider: FC = ({ children }) => {
    const [statusFilter, setStatusFilter] = useState<TASK_STATUS>(TASK_STATUS.TODO);
    return (
        <TodoFilterContext.Provider value={{statusFilter, setStatusFilter}}>
            { children }
        </TodoFilterContext.Provider>
    );
}
