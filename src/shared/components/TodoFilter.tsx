import React, { FC, useContext } from 'react';
import { Chip, makeStyles, Theme, Typography } from '@material-ui/core';
import { TASK_STATUS } from '../enums/task-status';
import { TodoFilterContext } from '../../context/todo-filter-context';

const useStyles = makeStyles((theme: Theme) => ({
    chip: {
        marginRight: 10,
    },
    title: {
        textAlign: "center",
        marginTop: 20,
        marginBottom: 20,
    },
}));

const TodoFilter: FC = () => {
    const classes = useStyles();
    const { statusFilter, setStatusFilter } = useContext(TodoFilterContext);
    return (
        <>
            <Typography className={classes.title} component="h1" variant="h5">
                Filtrar tareas
            </Typography>
            <Chip 
                color={statusFilter === TASK_STATUS.TODO ? 'primary' : 'default'}
                label="Todo" 
                onClick={() => setStatusFilter(TASK_STATUS.TODO)} 
                className={classes.chip}
            />
            <Chip 
                color={statusFilter === TASK_STATUS.DOING ? 'primary' : 'default'}
                label="Doing" 
                onClick={() => setStatusFilter(TASK_STATUS.DOING)} 
                className={classes.chip}
            />
            <Chip 
                color={statusFilter === TASK_STATUS.DONE ? 'primary' : 'default'}
                label="Done" 
                onClick={() => setStatusFilter(TASK_STATUS.DONE)} 
                className={classes.chip}
            />
        </>
    )
}

export default TodoFilter
