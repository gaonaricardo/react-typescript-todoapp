import React, { useState, useContext, ChangeEvent, FC } from 'react';
import { FormControl, TextField, makeStyles, Button, Typography } from '@material-ui/core';
import { v4 as uuid } from 'uuid';
import { ITodo } from '../interfaces/todo.interface';
import { TASK_STATUS } from '../enums/task-status';
import { TodoContext } from '../../context/todoapp-context';
import { addTodo } from '../helpers/actions-creators';

const useStyles = makeStyles((theme) => ({
    textField: {
        marginTop: 20
    },
    title: {
        textAlign: "center",
        marginTop: 20,
        marginBottom: 20,
    },
    form: {
        width: '100%',
    },
    button: {
        marginTop: 20,
        padding: 10,
    },
}));

const initialTodo: ITodo = {
    id: '',
    title: '',
    description: '',
    status: TASK_STATUS.TODO,
};

const TodoForm: FC = () => {
    const [todoObject, setTodoObject] = useState<ITodo>(initialTodo);
    const { dispatch } = useContext(TodoContext);
    const classes = useStyles();

    const updateField = (field: string, value: string): void => {
        setTodoObject({
            ...todoObject,
            [field]: value,
        });
    }

    const submit = () => {
        const newTodo: ITodo = {...todoObject, id: uuid()};
        dispatch(addTodo(newTodo));
        setTodoObject(initialTodo);
    }

    return (
    <>
        <Typography className={classes.title} component="h1" variant="h5">
            Agregar tarea
        </Typography>
        <FormControl className={classes.form}>
            <TextField
                variant="outlined"
                margin="normal"
                fullWidth
                id="todo-title"
                label="Todo title"
                name="todo-title"
                value={todoObject.title}
                onChange={(event: ChangeEvent<HTMLInputElement>) => updateField("title", event.currentTarget.value)}
            />
            <TextField
                variant="outlined"
                margin="normal"
                fullWidth
                id="todo-description"
                label="Todo description"
                name="todo-description"
                multiline
                minRows={3}
                maxRows={3}
                value={todoObject.description}
                onChange={(event: ChangeEvent<HTMLTextAreaElement>) => updateField("description", event.currentTarget.value)}
            />
            <Button 
                className={classes.button} 
                color="primary" 
                variant="contained"
                onClick={submit}
            >
                SAVE
            </Button>
        </FormControl>
    </>
    );
}

export default TodoForm;