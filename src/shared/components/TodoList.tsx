import React, { FC, useContext, useState } from 'react';
import { ITodo } from '../interfaces/todo.interface';
import { TASK_STATUS } from '../enums/task-status';
import { Typography, makeStyles, List, Accordion, AccordionDetails, AccordionSummary, Theme, Button, AccordionActions, Divider } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { TodoContext } from '../../context/todoapp-context';
import { startTodo, finishTodo } from '../helpers/actions-creators';
import { TodoFilterContext } from '../../context/todo-filter-context';

const useStyles = makeStyles((theme: Theme) => ({
    container: {
      paddingTop: 10,
    },
    title: {
      textAlign: "center",
      marginTop: 20,
    },
    todoItem: {
      width: '100%',
      backgroundColor: '#3F51B5',
      color: '#f2f2f2',
    },
    todoList: {
      marginTop: 20,
    },
    todoActionButton: {
      color: '#f2f2f2',
      borderColor: '#f2f2f2',
    },
  }))

const TodoList: FC = () => {
    const classes = useStyles();
    const { state:{todos}, dispatch } = useContext(TodoContext);
    const { statusFilter } = useContext(TodoFilterContext);
    const [expanded, setExpanded] = useState<string | false>();

    const handleChange = (panel: string) => (event: React.ChangeEvent<{}>, isExpanded: boolean) => {
        setExpanded(isExpanded ? panel : false);
      };

    return (
        <List className={classes.todoList}>
        {
          todos.map((todo: ITodo) => todo.status !== statusFilter ? null : (
            <Accordion 
                key={todo.id} 
                className={classes.todoItem} 
                expanded={expanded === todo.id}
                onChange={handleChange(todo.id)}
            >
                <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
                >
                <Typography>{ todo.title }</Typography>
                </AccordionSummary>
                <AccordionDetails>
                <Typography>
                    { todo.description }
                </Typography>
                </AccordionDetails>
                
                { todo.status !== TASK_STATUS.DONE &&
                <>
                    <Divider />
                    <AccordionActions>
                    <Button 
                        variant="outlined" 
                        className={classes.todoActionButton}
                        onClick={() => {
                            todo.status === TASK_STATUS.TODO ? dispatch(startTodo(todo)) : dispatch(finishTodo(todo))
                        setExpanded(false);
                        }}
                    >
                        {
                        todo.status === TASK_STATUS.TODO ? 'Start' : 'Finish'
                        }
                    </Button>
                    </AccordionActions>
                </>
                }
            </Accordion>
          ))
        }
        </List>
  );
}

export default TodoList;
