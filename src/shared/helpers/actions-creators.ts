import { ITodo } from '../interfaces/todo.interface';
import { Action } from '../interfaces/action.interface';
import { ACTION_TYPES } from '../enums/action-types';

export const addTodo = (payload: Partial<ITodo>): Action => ({
    type: ACTION_TYPES.ADD_TODO,
    payload,
});

export const startTodo = (payload: Partial<ITodo>): Action => ({
    type: ACTION_TYPES.START_TODO,
    payload,
});

export const finishTodo = (payload: Partial<ITodo>): Action => ({
    type: ACTION_TYPES.FINISH_TODO,
    payload,
});
