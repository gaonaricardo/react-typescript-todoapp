import { ACTION_TYPES } from '../enums/action-types';
import { ITodo } from '../interfaces/todo.interface';

export interface Action {
    type: ACTION_TYPES,
    payload: Partial<ITodo>
};