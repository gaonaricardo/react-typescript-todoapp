import { TASK_STATUS } from '../enums/task-status';

export interface ITodo {
    id: string;
    title: string;
    description: string;
    status: TASK_STATUS;
};
