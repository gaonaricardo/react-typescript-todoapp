import React, { FC } from 'react';
import TodoForm from './shared/components/TodoForm';
import TodoList from './shared/components/TodoList';
import { Container, makeStyles, Theme } from '@material-ui/core';
import { TodoProvider } from './context/todoapp-context';
import { TodoFilterProvider } from './context/todo-filter-context';
import TodoFilter from './shared/components/TodoFilter';

const useStyles = makeStyles((theme: Theme) => ({
	container: {
		paddingTop: 10,
	},
}));

const App: FC = () => {
	const classes = useStyles();

	return (
		<TodoProvider>
			<TodoFilterProvider>
				<Container className={classes.container} maxWidth="sm">
					<TodoForm />
					<TodoFilter />
					<TodoList />
				</Container>
			</TodoFilterProvider>
		</TodoProvider>
	);
}

export default App;
